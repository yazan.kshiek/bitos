package com.bitos.demo.unit.controler;

import com.bitos.demo.controller.DemoController;
import com.bitos.demo.exception.ExceptionUtil;
import com.bitos.demo.model.request.RegisterRequest;
import com.bitos.demo.security.CustomAccessDeniedHandler;
import com.bitos.demo.security.CustomAuthenticationEntryPoint;
import com.bitos.demo.service.CoinService;
import com.bitos.demo.service.UserService;
import com.bitos.demo.util.TokenHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@WebMvcTest(DemoController.class)
public class UserControllerTest {

    @MockBean
    CustomAccessDeniedHandler customAccessDeniedHandler;


    @MockBean
    CustomAuthenticationEntryPoint customAuthenticationEntryPoint;
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CoinService coinService;
    @MockBean
    private ExceptionUtil exceptionUtil;
    @MockBean
    private TokenHelper tokenHelper;
    @MockBean
    private UserService userService;


    @Test
    public void shouldReturnCoin() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/demo/public/coins")).andExpect(status().isOk())
                .andExpect(content().contentType("application/vnd.demo.api.v1+json"))
        .andReturn();
    }

    @Test
    public void shouldRegister() throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setName("u1");
        registerRequest.setEmail("u1@u1.com");
        registerRequest.setPassword("u1");

        MvcResult result = this.mockMvc
                .perform(
                        post("/demo/public/register")
                                .contentType("application/vnd.demo.api.v1+json")
                                .accept("application/vnd.demo.api.v1+json")
                        .content(objectMapper.writeValueAsString(registerRequest))


                )
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void registerValidation() throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setName(null);
        registerRequest.setEmail("u1@u1.com");
        registerRequest.setPassword("u1");

        MvcResult result = this.mockMvc
                .perform(
                        post("/demo/public/register")
                                .contentType("application/vnd.demo.api.v1+json")
                                .accept("application/vnd.demo.api.v1+json")
                                .content(objectMapper.writeValueAsString(registerRequest))


                )
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    //@WithMockUser("demo")
    public void tickerInfo() throws Exception {

        MvcResult result = this.mockMvc
                .perform(
                        get("/demo/ticker/"+"ZZZ")
                                .contentType("application/vnd.demo.api.v1+json")
                                .accept("application/vnd.demo.api.v1+json")

                )
                .andExpect(status().isOk())
                .andReturn();
    }
}
