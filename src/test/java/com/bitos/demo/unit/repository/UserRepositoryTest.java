package com.bitos.demo.unit.repository;

import com.bitos.demo.entity.User;
import com.bitos.demo.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class UserRepositoryTest {

    @MockBean
    private UserRepository userRepository;

    @Test
    public void testFindById() {

        User u = new User();
        u.setId(1L);
        u.setName("u");
        u.setEmail("u@u.com");
        u.setPassword("u");
        when(userRepository.findById(1L)).thenReturn(Optional.of(u));

    }
}
