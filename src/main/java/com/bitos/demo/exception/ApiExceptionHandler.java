package com.bitos.demo.exception;

import com.bitos.demo.model.response.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice(annotations = RestController.class)
@Slf4j
@Configuration
public class
ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    ExceptionUtil exceptionUtil;

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<ErrorResponse> handleGenericException(Exception ex) {
        log.error(ex.getMessage(), ex);
        return new ResponseEntity<>(
                exceptionUtil.getErrorResponse(ApiMessageCode.GENERAL_MSG), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {DataIntegrityViolationException.class})
    public ResponseEntity<ErrorResponse> handleGenericException(DataIntegrityViolationException ex) {
        log.error(ex.getMessage(), ex);
        Throwable cause = ex;
        while(cause.getCause() != null && cause.getCause() != cause) {
            cause = cause.getCause();
        }

        return new ResponseEntity<>(exceptionUtil
                .getErrorResponse(ApiMessageCode.DATA_INTEGRATION, cause.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = CustomException.class)
    public ResponseEntity<ErrorResponse> handleCustomException(CustomException ex) {
        log.error(ex.getMessage(), ex);
        return new ResponseEntity<>(exceptionUtil.getErrorResponse(ex), ex.getHttpStatus());
    }

    @ExceptionHandler(value = AccessDeniedException.class)
    public ResponseEntity<ErrorResponse> accessDenied(AccessDeniedException ex){
        log.error(ex.getMessage(), ex);
        return new ResponseEntity<>(
                exceptionUtil.getErrorResponse(ApiMessageCode.INVALID_REQUESTER), HttpStatus.FORBIDDEN);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
        HttpHeaders headers, HttpStatus status, WebRequest request) {

        log.error(ex.getMessage(), ex);
        List<String> errorMsg = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(fieldError ->
                        fieldError.getField() + ": " +
                                (fieldError.getDefaultMessage().contains("{")
                                        ? exceptionUtil.getErrorMsg(fieldError.getDefaultMessage()
                                            .replace("{", "").replace("}", ""))
                                        : fieldError.getDefaultMessage()))

                .collect(Collectors.toList());

        if(errorMsg.isEmpty())
            errorMsg.add(ex.getMessage());

        return new ResponseEntity<>(
                exceptionUtil.getErrorResponse(ApiMessageCode.INVALID_DATA, String.join(",", errorMsg)), status);
    }
}