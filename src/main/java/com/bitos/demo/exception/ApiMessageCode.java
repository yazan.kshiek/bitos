package com.bitos.demo.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ApiMessageCode {

    INVALID_REQUESTER("100", "API.INVALID_REQUESTER"),
    USER_EXIST("102", "API.USER.EXIST"),
    INVALID_CREDENTIAL("103", "API.INVALID.CREDENTIAL"),
    INVALID_COIN_CODE("104", "API.INVALID_COIN_CODE"),
    INVALID_DATA("105", "API.INVALID.DATA"),
    GENERAL_MSG("106", "API.GENERAL_MSG"),
    DATA_INTEGRATION("107", "API.DATA_INTEGRATION"),
    EXTERNAL_ERROR_4XX("108", "API.EXTERNAL_ERROR_4XX"),
    EXTERNAL_ERROR_5XX("109", "API.EXTERNAL_ERROR_5XX"),
    NOT_ACCEPTABLE("110","API.NOT_ACCEPTABLE");

    private String code;
    private String message;
}
