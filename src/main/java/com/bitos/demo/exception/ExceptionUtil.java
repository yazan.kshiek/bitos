package com.bitos.demo.exception;

import com.bitos.demo.model.response.ErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Locale;

@Component
public class ExceptionUtil {

    @Autowired
    @Qualifier("messageSource")
    private MessageSource msgSrc;


    public ErrorResponse getErrorResponse(ApiMessageCode messageCode){
        return ErrorResponse.builder().error(messageCode.getCode())
                .errorMessage(msgSrc.getMessage(messageCode.getMessage(), null, Locale.ENGLISH))
                .timestamp(new Date()).build();
    }

    public ErrorResponse getErrorResponse(ApiMessageCode messageCode, String errorMessage){
        return ErrorResponse.builder().error(messageCode.getCode())
                .errorMessage(errorMessage)
                .timestamp(new Date()).build();
    }

    public ErrorResponse getErrorResponse(CustomException e){
        return ErrorResponse.builder().error(e.getError()).errorMessage(e.getErrorMessage()).timestamp(new Date()).build();
    }

    public CustomException getCustomException(ApiMessageCode messageCode, String errorMessage, HttpStatus status){
        return CustomException.builder().error(messageCode.getCode()).errorMessage(errorMessage).timestamp(new Date())
                .httpStatus(status).build();
    }

    public CustomException getCustomException(ApiMessageCode messageCode, HttpStatus status){
        return CustomException.builder().error(messageCode.getCode())
                .errorMessage(msgSrc.getMessage(messageCode.getMessage(), null, Locale.ENGLISH))
                .timestamp(new Date())
                .httpStatus(status).build();
    }

    public String getErrorMsg(String msgCode){
        return msgSrc.getMessage(msgCode, null, Locale.ENGLISH);
    }
}
