package com.bitos.demo.exception;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.http.HttpStatus;

import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CustomException extends Exception {

    private String error;
    private String errorMessage;
    private Date timestamp;

    @JsonIgnore
    private HttpStatus httpStatus;



    @Override
    public String toString(){
        return "{\"error\":\"" + error + "\",\"errorMessage\":\"" + errorMessage + "\",\"timestamp\":\"" + timestamp + "\"}";
    }
}
