package com.bitos.demo;

public enum Sorting {

    ASC("ASC"),
    DESC("DESC");

    private String value;

    Sorting(String value){
        this.value=value;
    }
}
