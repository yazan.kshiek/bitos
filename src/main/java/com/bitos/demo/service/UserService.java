package com.bitos.demo.service;

import com.bitos.demo.entity.User;
import com.bitos.demo.exception.ApiMessageCode;
import com.bitos.demo.exception.CustomException;
import com.bitos.demo.exception.ExceptionUtil;
import com.bitos.demo.model.request.AuthenticationRequest;
import com.bitos.demo.model.request.RegisterRequest;
import com.bitos.demo.model.response.AuthenticationResponse;
import com.bitos.demo.repository.DbRepository;
import com.bitos.demo.repository.UserRepository;
import com.bitos.demo.util.TokenHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class UserService extends DbService<User> {

    @Autowired
    TokenHelper tokenHelper;
    @Autowired
    ExceptionUtil exceptionUtil;
    @Autowired
    private UserRepository userRepository;
    @Autowired private PasswordEncoder encoder;

    @Override
    public DbRepository<User> getRepository() {
        return userRepository;
    }

    @Transactional
    public User register(RegisterRequest registerRequest) throws CustomException {

        Optional<User> maybeUser = userRepository.findByName(registerRequest.getName());
        if(maybeUser.isPresent())
            throw exceptionUtil.getCustomException(ApiMessageCode.USER_EXIST, HttpStatus.BAD_REQUEST);

        User user = new User();
        user.setName(registerRequest.getName());
        user.setEmail(registerRequest.getEmail());
        user.setPassword(encoder.encode(registerRequest.getPassword()));
        return userRepository.save(user);
    }

    public AuthenticationResponse auth(AuthenticationRequest request) throws CustomException {

        Optional<User> maybeUser = userRepository.findByName(request.getUsername());
        if(!maybeUser.isPresent() || !encoder.matches(request.getPassword(), maybeUser.get().getPassword()))
            throw exceptionUtil.getCustomException(ApiMessageCode.INVALID_CREDENTIAL, HttpStatus.FORBIDDEN);

        // generate token
        return AuthenticationResponse.builder().accessToken(tokenHelper.createToken(maybeUser.get().getId().toString())).build();
    }
}
