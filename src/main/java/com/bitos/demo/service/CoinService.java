package com.bitos.demo.service;

import com.bitos.demo.exception.ApiMessageCode;
import com.bitos.demo.exception.ExceptionUtil;
import com.bitos.demo.model.response.AlternativeMeCoinResponse;
import com.bitos.demo.model.response.CoinResponse;
import com.bitos.demo.model.response.TickerInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.wnameless.json.flattener.JsonFlattener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CoinService {

    public static final String SYMBOL_PATH = "data.@id@.symbol";
    public static final String LAST_UPDATE_PATH = "data.@id@.last_updated";
    public static final String PRICE_PATH = "data.@id@.quotes.USD.price";
    public static final String VOLUME_PATH = "data.@id@.quotes.USD.volume_24h";
    public static final String DAILY_CHANGE_PATH = "data.@id@.quotes.USD.percentage_change_24h";
    private static final Duration REQUEST_TIMEOUT = Duration.ofSeconds(50);
    @Autowired
    ExceptionUtil exceptionUtil;
    @Autowired
    private WebClient webClient;

    @Cacheable(value = "coin", cacheManager = "alternateCacheManager")
    public List<CoinResponse> getAll() throws JsonProcessingException {

        String responseJson = webClient
                .get()
                .uri("listings/")
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        error -> Mono.error(
                                exceptionUtil.getCustomException(ApiMessageCode.EXTERNAL_ERROR_4XX, HttpStatus.NOT_FOUND)))

                .onStatus(HttpStatus::is5xxServerError,
                        error -> Mono.error(
                                exceptionUtil.getCustomException(ApiMessageCode.EXTERNAL_ERROR_5XX, HttpStatus.INTERNAL_SERVER_ERROR)))
                .bodyToMono(String.class)
                .block(REQUEST_TIMEOUT);

        if (!responseJson.endsWith("}"))
            responseJson = responseJson + "}";

        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> map = objectMapper.readValue(responseJson, Map.class);

        List<LinkedHashMap<String, String>> data = (List<LinkedHashMap<String, String>>) map.get("data");

        List<AlternativeMeCoinResponse> convertedData =
                data.stream().map(m -> objectMapper.convertValue(m, AlternativeMeCoinResponse.class)).collect(Collectors.toList());

        return convertedData.stream()
                .map(CoinResponse::get)
                .collect(Collectors.toList());
    }

    @Cacheable(value = "ticker", key = "#coinResponse.code")
    public TickerInfo getTickerInfo(CoinResponse coinResponse){


        String responseJson = webClient
                .get()
                .uri("ticker/" + coinResponse.getId() + "/")
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        error -> Mono.error(
                                exceptionUtil.getCustomException(ApiMessageCode.EXTERNAL_ERROR_4XX, HttpStatus.NOT_FOUND)))

                .onStatus(HttpStatus::is5xxServerError,
                        error -> Mono.error(
                                exceptionUtil.getCustomException(ApiMessageCode.EXTERNAL_ERROR_5XX, HttpStatus.INTERNAL_SERVER_ERROR)))
                .bodyToMono(String.class)
                .block(REQUEST_TIMEOUT);

        if (!responseJson.endsWith("}"))
            responseJson = responseJson + "}";

        Map<String, Object> flattenJson = JsonFlattener.flattenAsMap(responseJson);

        TickerInfo tickerInfo = new TickerInfo();
        tickerInfo.setCode((String) flattenJson.get(SYMBOL_PATH.replace("@id@", coinResponse.getId().toString())));

        tickerInfo.setLast_updated(new Long(flattenJson.get(LAST_UPDATE_PATH.replace("@id@", coinResponse.getId().toString())).toString()));

        tickerInfo.setPrice(new Double(flattenJson.get(PRICE_PATH.replace("@id@", coinResponse.getId().toString())).toString()));
        tickerInfo.setVolume(new Double(flattenJson.get(VOLUME_PATH.replace("@id@", coinResponse.getId().toString())).toString()));

        Object dailyPath = flattenJson.get(DAILY_CHANGE_PATH.replace("@id@", coinResponse.getId().toString()));
        if (dailyPath != null) {
            tickerInfo.setDaily_change(new Double(dailyPath.toString()));
        }
        return tickerInfo;
    }
}
