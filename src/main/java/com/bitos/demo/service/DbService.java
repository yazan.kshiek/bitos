package com.bitos.demo.service;

import com.bitos.demo.entity.DbEntity;
import com.bitos.demo.repository.DbRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public abstract class DbService<T extends DbEntity> {

    public abstract DbRepository<T> getRepository();

    @Autowired
    @Qualifier("messageSource")
    protected MessageSource msgSrc;

    public Optional<T> getById(Long id){
        return getRepository().findById(id);
    }
}