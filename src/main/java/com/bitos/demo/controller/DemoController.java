package com.bitos.demo.controller;

import com.bitos.demo.Sorting;
import com.bitos.demo.entity.User;
import com.bitos.demo.exception.ApiMessageCode;
import com.bitos.demo.exception.CustomException;
import com.bitos.demo.exception.ExceptionUtil;
import com.bitos.demo.model.request.AuthenticationRequest;
import com.bitos.demo.model.request.RegisterRequest;
import com.bitos.demo.model.response.AuthenticationResponse;
import com.bitos.demo.model.response.CoinResponse;
import com.bitos.demo.model.response.TickerInfo;
import com.bitos.demo.service.CoinService;
import com.bitos.demo.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/demo")
public class DemoController {


    @Autowired
    UserService userService;
    @Autowired
    private ExceptionUtil exceptionUtil;
    @Autowired
    private CoinService coinService;

    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "User registered",
                    content = @Content(mediaType = "application/vnd.demo.api.v1+json")),
            @ApiResponse(
                    responseCode = "400",
                    description = "Unaccepted username",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(
                    responseCode = "500",
                    description = "Server error",
                    content = @Content(mediaType = "application/json"))
    })

    @RequestMapping(value = "/public/register", method = RequestMethod.POST, produces = "application/vnd.demo.api.v1+json")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<User> register(@RequestBody @Valid RegisterRequest registerRequest) throws CustomException {
        return new ResponseEntity<>(userService.register(registerRequest), HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Authenticated successfully",
                    content = @Content(mediaType = "application/vnd.demo.api.v1+json")),
            @ApiResponse(
                    responseCode = "404",
                    description = "Unauthenticated user",
                    content = @Content(mediaType = "application/json")),
            @ApiResponse(
                    responseCode = "500",
                    description = "Server error",
                    content = @Content(mediaType = "application/json"))
    })
    @RequestMapping(value = "/public/auth", method = RequestMethod.POST, produces = "application/vnd.demo.api.v1+json")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<AuthenticationResponse> auth(@RequestBody @Valid AuthenticationRequest request) throws CustomException {
        return new ResponseEntity<>(userService.auth(request), HttpStatus.OK);
    }


    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully",
                    content = @Content(mediaType = "application/vnd.demo.api.v1+json")),
            @ApiResponse(
                    responseCode = "500",
                    description = "Server error",
                    content = @Content(mediaType = "application/json"))
    })
    @ResponseStatus(code = HttpStatus.OK)
    @RequestMapping(value = "/public/coins", method = RequestMethod.GET, produces = "application/vnd.demo.api.v1+json")
    public List<CoinResponse> coins(@RequestParam(name = "sort", required = false, defaultValue = "ASC") Sorting sort) throws JsonProcessingException {
        List<CoinResponse> data = coinService.getAll();
        return data.stream().sorted((c1, c2) ->
                sort.equals(Sorting.ASC)
                        ? c1.getName().compareTo(c2.getName())
                        : c1.getName().compareTo(c2.getName()) * -1
        ).collect(Collectors.toList());
    }

    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Successfully",
                    content = @Content(mediaType = "application/vnd.demo.api.v1+json")),
            @ApiResponse(
                    responseCode = "500",
                    description = "Server error",
                    content = @Content(mediaType = "application/json"))
    })
    @ResponseStatus(code = HttpStatus.OK)
    @RequestMapping(value = "/ticker/{coinCode}", method = RequestMethod.GET, produces = "application/vnd.demo.api.v1+json")
    public TickerInfo ticker(@PathVariable(name = "coinCode") String coinCode) throws JsonProcessingException, CustomException {

        Optional<CoinResponse> maybeCoin = coinService.getAll().stream().filter(item -> item.getCode().equals(coinCode)).findAny();
        if (!maybeCoin.isPresent())
            throw exceptionUtil.getCustomException(ApiMessageCode.INVALID_COIN_CODE, HttpStatus.BAD_REQUEST);

        return coinService.getTickerInfo(maybeCoin.get());
    }
}
