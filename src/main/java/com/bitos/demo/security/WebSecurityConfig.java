package com.bitos.demo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.security.SecureRandom;

import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;


@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
    private CustomAuthenticationEntryPoint customAuthenticationEntryPoint;
	@Autowired
    private TokenRequestFilter tokenRequestFilter;
	@Autowired
    private CustomAccessDeniedHandler customAccessDeniedHandler;



	@Bean
	public PasswordEncoder encoder() {
	    return new BCryptPasswordEncoder(15, new SecureRandom());
	}

	@Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers(
            "/api-docs/",
            "/configuration/ui",
            "/swagger-resources/**",
            "/swagger-ui/**",
            "/configuration/security",
            "/swagger-ui-custom.html",
            "/webjars/**",
            "/**/public/**");
    }
	
	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {

		httpSecurity.cors().and().csrf().disable().authorizeRequests()

                .antMatchers(
                        "/swagger-ui-custom.html",
                        "/swagger-ui/**",
                        "/api-docs/**",
                        "/configuration/ui",
                        "/swagger-resources/**",
                        "/swagger-ui/**",
                        "/configuration/security",
                        "/swagger-ui-custom.html",
                        "/webjars/**",
                        "/**/public/**"
                ).permitAll()
                .anyRequest().authenticated().and()
				.exceptionHandling()
					.authenticationEntryPoint(customAuthenticationEntryPoint)
					.accessDeniedHandler(customAccessDeniedHandler)
				.and().sessionManagement().sessionCreationPolicy(STATELESS);

		httpSecurity.addFilterBefore(tokenRequestFilter, UsernamePasswordAuthenticationFilter.class);
	}
}
