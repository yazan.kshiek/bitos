package com.bitos.demo.security;

import com.bitos.demo.exception.ApiMessageCode;
import com.bitos.demo.exception.ExceptionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

import static org.springframework.http.HttpStatus.FORBIDDEN;

@Component
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {

    @Autowired
    private ExceptionUtil exceptionUtil;

    @Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException authException) throws IOException {

        String body = exceptionUtil.getCustomException(ApiMessageCode.INVALID_REQUESTER, FORBIDDEN).toString();

        response.setStatus(FORBIDDEN.value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.getWriter().write(body);
	}
}