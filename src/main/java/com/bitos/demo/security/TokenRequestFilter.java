package com.bitos.demo.security;

import com.bitos.demo.entity.User;
import com.bitos.demo.exception.ApiMessageCode;
import com.bitos.demo.exception.ExceptionUtil;
import com.bitos.demo.service.UserService;
import com.bitos.demo.util.TokenHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static org.springframework.security.core.context.SecurityContextHolder.getContext;

@Slf4j
@Order(1)
@Component
public class TokenRequestFilter extends OncePerRequestFilter {

    @Autowired
    TokenHelper tokenHelper;

    @Autowired
    ExceptionUtil exceptionUtil;

    @Autowired
    UserService userService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        try {

            Optional<String> token = tokenHelper.extractToken(request);
            Long userId = token.isPresent() ? tokenHelper.extractUserId(token.get()) : null;
            if (userId != null && getContext().getAuthentication() == null) {
                Optional<User> upMaybe = userService.getById(userId);
                if (upMaybe.isPresent() && tokenHelper.validateToken(token.get(), upMaybe.get())) {
                    UsernamePasswordAuthenticationToken userPassAuthToken = new UsernamePasswordAuthenticationToken(
                            upMaybe.get(), null, null);

                    userPassAuthToken.setDetails(userId);
                    getContext().setAuthentication(userPassAuthToken);
                }
            }
            chain.doFilter(request, response);
        } catch (ExpiredJwtException ex) {
            log.info("expired {}", ex.getMessage());
            invalidRequester(response);
        }
    }

    private void invalidRequester(HttpServletResponse response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String body = mapper.writeValueAsString(exceptionUtil.getErrorResponse(ApiMessageCode.INVALID_REQUESTER));
        response.setStatus(HttpStatus.FORBIDDEN.value());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.getWriter().write(body);
    }
}
