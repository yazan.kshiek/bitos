package com.bitos.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@MappedSuperclass
public abstract class DbEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "IdGenerator")
    @GenericGenerator(name = "IdGenerator", strategy = "com.bitos.demo.entity.IdGenerator")
    @Column(name = "ID")
    private Long id;
}
