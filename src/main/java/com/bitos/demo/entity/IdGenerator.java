package com.bitos.demo.entity;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;
import java.util.List;

public class IdGenerator implements IdentifierGenerator {

    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object obj) throws HibernateException {
        // TODO Auto-generated method stub
        String query = String.format("select max(%s) from %s ",
                session.getEntityPersister(obj.getClass().getName(), obj).getIdentifierPropertyName(),
                obj.getClass().getSimpleName());

        List<?> results = session.createQuery(query).getResultList();

        if (results == null || results.size() == 0 || results.get(0) == null)
            return 1L;

        return Long.parseLong(results.get(0).toString()) + 1;
    }
}
