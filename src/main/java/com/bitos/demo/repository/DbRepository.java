package com.bitos.demo.repository;

import com.bitos.demo.entity.DbEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface DbRepository<T extends DbEntity> extends JpaRepository<T, Long> {}