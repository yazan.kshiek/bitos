package com.bitos.demo.repository;

import com.bitos.demo.entity.User;

import java.util.Optional;

public interface UserRepository extends DbRepository<User> {

    Optional<User> findByName(String name);
}