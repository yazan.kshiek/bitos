package com.bitos.demo.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ErrorResponse implements Serializable {

    private String error;
    private String errorMessage;
    private Date timestamp;
}
