package com.bitos.demo.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TickerInfo {

    private String code;
    private Double price;
    private Double volume;
    private Long last_updated;
    private Double daily_change;
}
