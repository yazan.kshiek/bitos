package com.bitos.demo.model.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CoinResponse {
    @JsonIgnore
    private Long id;
    private String name;
    private String code;

    public static CoinResponse get(AlternativeMeCoinResponse res){
        return CoinResponse.builder().code(res.getSymbol()).name(res.getName()).id(res.getId()).build();
    }
}
