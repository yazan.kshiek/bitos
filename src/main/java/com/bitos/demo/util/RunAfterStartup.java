package com.bitos.demo.util;

import com.bitos.demo.service.CoinService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class RunAfterStartup {

    @Autowired
    CoinService coinService;


    @EventListener(ApplicationReadyEvent.class)
    public void runAfterStartup() throws JsonProcessingException {
        coinService.getAll();
    }
}
