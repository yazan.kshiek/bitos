package com.bitos.demo.util;

import com.bitos.demo.entity.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Optional;

import static org.joda.time.DateTimeUtils.currentTimeMillis;

@Component
public class TokenHelper {

    public static final String BEARER = "Bearer ";
    public static final String AUTH_HEADER = "Authorization";
    @Value("${jwt.key}") private String SECRET_KEY;
    @Value("${jwt.key.expiry.in.hours:1}") private int JWT_EXPIRY;

    public Optional<String> extractToken(HttpServletRequest request) {
        final String authorizationHeader = request.getHeader(AUTH_HEADER);
        if (authorizationHeader != null && authorizationHeader.startsWith(BEARER)) {
            return Optional.of(authorizationHeader.substring(7));
        }
        return Optional.empty();
    }

    public Long extractUserId(String token) {
        Claims body = Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();

        return Long.parseLong(body.getSubject());
    }

    public String createToken(String subject) {
        Date it = new Date(currentTimeMillis());
        Date exp = new Date(currentTimeMillis() + 1000 * 60 * 60 * JWT_EXPIRY);

        return Jwts.builder()
                .setSubject(subject)
                .setIssuedAt(it)
                .setExpiration(exp)
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
                .compact();
    }

    public Boolean validateToken(String token, User userDetails) {
        Long userId = extractUserId(token);
        return userId.equals(userDetails.getId());
    }
}
